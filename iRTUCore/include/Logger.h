/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef LOGGER_C
#define LOGGER_C

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))

typedef enum LogLevel
{
	LOG_INFO = 0,
	LOG_ERROR = 1,
	LOG_WARNING = 2,
	LOG_CRITICAL = 3,
	LOG_PANIC = 4
}LogLevel;

extern LIBRARY_EXPORT size_t  logger_allocate_default();
extern LIBRARY_EXPORT size_t  logger_allocate(size_t flszmb, const char* dirpath);
extern LIBRARY_EXPORT void    logger_release(size_t loggerid);
extern LIBRARY_EXPORT bool    logger_start_logging(size_t loggerid);
extern LIBRARY_EXPORT void    logger_stop_logging(size_t loggerid);
extern LIBRARY_EXPORT bool    logger_write(size_t loggerid, const char* logentry, LogLevel llevel, const char* func, const char* file, int line);
extern LIBRARY_EXPORT size_t  logger_get_instance();

#define WriteLog(id, str, level) \
    logger_write(id, str, level, __PRETTY_FUNCTION__, __FILE__, __LINE__)

#define WriteInformation(id, str) \
    logger_write(id, str, LOG_INFO, __PRETTY_FUNCTION__, __FILE__, __LINE__);

#ifdef __cplusplus
}
#endif

#endif

