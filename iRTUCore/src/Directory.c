/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#include "Directory.h"

#define DIRECTORY_SEPARATOR '/'

#include <sys/stat.h>

#include <stdlib.h>
#include <memory.h>
#include <dirent.h>
#include <unistd.h>

char* dir_get_parent_directory(const char* dirname)
{
	size_t origlen = strlen(dirname);

	char* parent_dir = (char*)calloc(1, sizeof(char) * (origlen + 1));

	if(parent_dir == NULL)
	{
		return NULL;
	}

	memcpy(parent_dir, dirname, origlen);

	int len = (int)strlen(parent_dir);

	if(len < 2)
    {
        free(parent_dir);
		return NULL;
    }

	int ctr = len - 1;

	while(true)
	{
		parent_dir[ctr] = 0;
		ctr--;
		if(parent_dir[ctr] == '/' || parent_dir[ctr] == '\\')
		{
			break;
		}
	}

	return parent_dir;
}

bool dir_is_exists(const char* dirname)
{
	DIR* dirp;

	dirp = opendir(dirname);

	if(dirp == NULL)
	{
		closedir(dirp);
		return false;
	}

	closedir(dirp);

	return true;
}

bool dir_create_directory(const char* dirname)
{
	return mkdir(dirname, S_IRWXU);
}
