#ifndef IRTU_COMMON
#define IRTU_COMMON

#include <string>
#include <map>
#include <vector>

class Configuration
{
public:
    Configuration();
    ~Configuration();
    void SetFileName(std::string fname);
    bool LoadConfiguration();
    bool LoadCustomConfiguration(const std::string &configFile);
    bool GetValue(const std::string &section, const std::string &settingKey, std::string &value);
    bool GetValueList(const std::string &section, const std::string &settingKey, const char delim, std::vector<std::string> &valuelist);
    bool IsSection(const std::string &section);
private:
    bool LoadConfiguration(const std::string &configFile);
    bool AddSection(std::string &str, const std::map<std::string, std::string> &list);
    std::map<std::string, std::map<std::string, std::string>> _ConfigurationMap;
    std::string _ConfigFileName;
};

void SplitString(const std::string &str, const char delim, std::string &keystr, std::string &valuestr);
void SplitString(const std::string &str, const char delim, std::vector<std::string> &tokens);
void ReplaceString(std::string &srcstr, const std::string oldpattern, const std::string newpattern);
void RealToString(std::string &str, const double val);
void IntegerToString(std::string &str, const long val);
void TrimString(std::string &str);
void GetParentDirectory(char *ptr);
std::string GetTimeStampString();

#endif
