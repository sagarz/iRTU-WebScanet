#include "iRTUCommon.hpp"
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <unistd.h>
#include <vector>
#include <fstream>
#include <sstream>

Configuration::Configuration()
{
    _ConfigFileName = "";
}

Configuration::~Configuration()
{
}

void Configuration::SetFileName(std::string fname)
{
    _ConfigFileName = fname;
}

bool Configuration::IsSection(const std::string &section)
{
    std::map<std::string, std::map<std::string, std::string>>::const_iterator confmapiter;

    confmapiter = _ConfigurationMap.find(section);
    if(confmapiter == _ConfigurationMap.end())
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool Configuration::GetValue(const std::string &section, const std::string &settingKey, std::string &value)
{
    std::map<std::string, std::map<std::string, std::string>>::const_iterator confmapiter;
    std::map<std::string, std::string>::const_iterator kviter;

    confmapiter = _ConfigurationMap.find(section);
    if(confmapiter == _ConfigurationMap.end())
    {
        return false;
    }
    else
    {
        std::map<std::string, std::string> list = confmapiter->second;
        kviter = list.find(settingKey);

        if(kviter == list.end())
        {
            return false;
        }

        value = (std::string)kviter->second;
    }

    return true;
}

bool Configuration::GetValueList(const std::string &section, const std::string &settingKey, const char delim, std::vector<std::string> &valuelist)
{
    std::map<std::string, std::map<std::string, std::string>>::const_iterator confmapiter;
    std::map<std::string, std::string>::const_iterator kviter;
    std::string value;

    confmapiter = _ConfigurationMap.find(section);
    if(confmapiter == _ConfigurationMap.end())
    {
        return false;
    }
    else
    {
        std::map<std::string, std::string> list = confmapiter->second;
        kviter = list.find(settingKey);

        if(kviter == list.end())
        {
            return false;
        }

        value = (std::string)kviter->second;

        SplitString(value, delim, valuelist);
    }

    return true;
}

bool Configuration::LoadCustomConfiguration(const std::string &configFile)
{
    return LoadConfiguration(configFile);
}

bool Configuration::LoadConfiguration()
{
    char filepathbuffer[1024];
    memset((char*)&filepathbuffer[0],0,1024);
    getcwd(&filepathbuffer[0],1024);
    GetParentDirectory(&filepathbuffer[0]);

    strcat(filepathbuffer,"etc");
    filepathbuffer[strlen(filepathbuffer)] = '/';
    strcat(filepathbuffer, _ConfigFileName.c_str());

    if(!LoadConfiguration(filepathbuffer))
    {
            return false;
    }

    return true;
}

bool Configuration::LoadConfiguration(const std::string &configFile)
{
    std::ifstream cfgfile(configFile.c_str());
    std::string line, leftstr, rightstr;
    std::vector<std::string> linelist;

    // Following is a Windows INI style configuration file parsing algorithm
    // The first iteration only loads relevent lines from as a list of strings
    if(!cfgfile.is_open())
    {
        return false;
    }
    else
    {
        while(cfgfile.good())
        {
              line.erase();
              std::getline(cfgfile,line);
              TrimString(line);

              if(line.length() < 1 || line[0]==';' || line[0]=='#' || line.empty())
              {
                  //Skip comment or blank lines;
                  continue;
              }

              if(!isalnum(line[0]))
              {
                  if(line[0]=='[' && line[line.length()-1]==']')
                  {
                      //Section header
                      linelist.push_back(line);
                  }
                  //Garbage or Invalid line
                  continue;
              }
              else
              {
                  //Normal line
                  linelist.push_back(line);
              }
        }
        // The file can be closed off
        cfgfile.close();
    }

    //Now we would iterate the string list and segregate key value pairs by section groups
    std::string curSecHeader = "";
    std::map<std::string, std::string> kvlist;

    for(std::vector<std::string>::size_type i = 0; i != linelist.size(); i++)
    {
        line = linelist[i];
        //Section header line
        if(line[0]=='[' && line[line.length()-1]==']')
        {
            //Check whether this is the first instance of a section header
            if(_ConfigurationMap.size()<1)
            {
                if(curSecHeader.length() > 0)
                {
                    //We reach here when a section is being read for the first time
                    if(!AddSection(curSecHeader,kvlist))
                    {
                        return false;
                    }
                }
            }
            else
            {
                //Before staring a new section parsing we need to store the last one
                if(!AddSection(curSecHeader,kvlist))
                {
                    return false;
                }
            }

            //Store the string as current section header and clear the key value list
            curSecHeader = line;
            kvlist.clear();
        }
        else
        {
            leftstr = rightstr = "";
            SplitString(line,'=', leftstr, rightstr);
            TrimString(leftstr);
            TrimString(rightstr);
            kvlist[leftstr]=rightstr;
        }
    }

    if(!AddSection(curSecHeader,kvlist))
    {
        return false;
    }

    return true;
}

bool Configuration::AddSection(std::string &str, const std::map<std::string, std::string> &list)
{
    if(str[0] != '[' || str[str.length() - 1] != ']')
    {
        return false;
    }

    str.erase(0,1);
    str.erase(str.length()-1,1);
    _ConfigurationMap[str] = list;

    return true;
}

void SplitString(const std::string &str, const char delim, std::string &keystr, std::string &valuestr)
{
    std::stringstream ss(str); //convert string to stream
    std::string item;

    int ctr = 0;

    while(getline(ss, item, delim))
    {
        if(ctr==0)
        {
            keystr = item;
            ctr++;
            continue;
        }

        if(ctr==1)
        {
            valuestr = item;
            ctr++;
            continue;
        }

        if(ctr>1)
        {
            valuestr += delim;
            valuestr += item;
            ctr++;
        }
    }
}

void SplitString(const std::string &str, const char delim, std::vector<std::string> &tokens)
{
    std::stringstream ss(str); //convert string to stream
    std::string item;

    while(getline(ss, item, delim))
    {
        tokens.push_back(item); //add token to vector
    }
}

void TrimString(std::string &str)
{
    char buffer[4096];
    memset((char*)&buffer,0,4096);
    strcpy(buffer,str.c_str());

    size_t len = strlen(buffer);

    if(len<1)
        return;

    for(size_t i = len-1;  ; i--)
    {
        if(!isspace(buffer[i]) || i <= 0)
        {
            break;
        }
        buffer[i] = '\0';
    }

    len = strlen(buffer);

    if(len<1)
    {
        str = buffer;
        return;
    }

    const char *buf=(const char*)&buffer[0];

    for ( NULL; *buf && isspace(*buf); buf++);

    str = buf;
}

void GetParentDirectory(char *ptr)
{
    if(ptr == nullptr)
        return;

    int len = strlen(ptr);

    if(len < 2)
        return;

    int ctr = len-1;

    while(true)
    {
        ptr[ctr] = 0;
        ctr--;
        if(ptr[ctr]== '/')
        {
            break;
        }
    }
}

std::string GetTimeStampString()
{
    char buffer[33] = {0};

    time_t t ;
    struct tm *tmp ;
    time(&t);
    tmp = localtime(&t);

    sprintf(buffer, "%04d%02d%02d%02d%02d%02d",
             (tmp->tm_year+1900), (tmp->tm_mon+1),tmp->tm_mday,
             tmp->tm_hour, tmp->tm_min, tmp->tm_sec);

    return buffer;
}

void ReplaceString(std::string &srcstr, const std::string oldpattern, const std::string newpattern)
{
    if (oldpattern.length() == 0 || srcstr.length() == 0)
    {
        return;
    }

    size_t idx = 0;

    for(;;)
    {
        idx = srcstr.find( oldpattern, idx);

        if (idx == std::string::npos)
        {
            break;
        }

        srcstr.replace( idx, oldpattern.length(), newpattern);
        idx += newpattern.length();
    }
}

void RealToString(std::string &str, const double val)
{
    char ptr[33] = {0};
    sprintf(ptr,"%10.6lf",val);
    str = ptr;
}

void IntegerToString(std::string &str, const long val)
{
    char ptr[33] = {0};
    sprintf(ptr,"%ld",val);
    str = ptr;
}
