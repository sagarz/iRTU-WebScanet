#include "EthernetService.hpp"

int main(int argc, char* argv[])
{
    EthernetService eth;

    if(!eth.Initialize())
    {
        return -1;
    }

    eth.Start();

    return 0;
}
