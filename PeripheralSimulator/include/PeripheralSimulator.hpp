#ifndef PERIPHERAL_SIMULATOR
#define PERIPHERAL_SIMULATOR

#include <stdbool.h>
#include <string>
#include <vector>
#include "iRTUCommon.hpp"

class PeripheralSimulator
{
public:
    PeripheralSimulator();
    virtual ~PeripheralSimulator();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    long GetLogger();
private:
    Configuration cfg;
    void* message_bus;
    long logger_id;
    std::vector<std::string> destinationlist;
};

#endif
