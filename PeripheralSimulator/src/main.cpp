#include "PeripheralSimulator.hpp"

int main(int argc, char* argv[])
{
    PeripheralSimulator simulator;

    if(!simulator.Initialize())
    {
        return -1;
    }

    simulator.Start();

    return 0;
}
