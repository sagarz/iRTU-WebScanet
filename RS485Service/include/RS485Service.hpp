#ifndef RS485_SERVICE
#define RS485_SERVICE

#include <stdbool.h>
#include <string>
#include <vector>
#include "iRTUCommon.hpp"
#include "IProtocolRS485.hpp"

class RS485Service
{
public:
    RS485Service();
    virtual ~RS485Service();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    long GetLogger();
private:
    Configuration cfg;
    void* message_bus;
    long logger_id;
    std::vector<std::string> destinationlist;

    IProtocolRS485 *io_protocol;

    bool enabled;
    std::string device_id;
    std::string protocol_name;
    std::string device;
    int baud_rate;
    char parity;
    int data_bits;
    int stop_bits;
    int xon_off;
};

#endif
