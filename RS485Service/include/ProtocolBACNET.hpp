#ifndef _PROTOCOL_BACNET
#define _PROTOCOL_BACNET

#include "IProtocolRS485.hpp"

class ProtocolBacnet : public IProtocolRS485
{
public:
    ProtocolBacnet();
    virtual ~ProtocolBacnet() override;
    bool SetDeviceName(const std::string &device_name, unsigned char slaveid) override;
    bool SetDeviceConfiguration(int bd, char pr, int sb, int db, int xo) override;
    bool Initialize() override;
    bool Destroy() override;
    bool Read(unsigned int index, void** buffer) override;
    bool Write(unsigned int index, void* buffer) override;
};

#endif
