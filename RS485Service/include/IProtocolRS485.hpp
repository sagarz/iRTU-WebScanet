#ifndef I_PROTOCOL_RS485
#define I_PROTOCOL_RS485

#include <string>

class IProtocolRS485
{
    public:
        IProtocolRS485() {}
        virtual ~IProtocolRS485() {}
        virtual bool SetDeviceName(const std::string &device_name, unsigned char slaveid) = 0;
        virtual bool SetDeviceConfiguration(int bd, char pr, int sb, int db, int xo) =0;
        virtual bool Initialize() = 0;
        virtual bool Destroy() = 0;
        virtual bool Read(unsigned int index, void** buffer) = 0;
        virtual bool Write(unsigned int index, void* buffer) = 0;
};

#endif
