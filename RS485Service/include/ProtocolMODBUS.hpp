#ifndef _PROTOCOL_MODBUS
#define _PROTOCOL_MODBUS

#include "IProtocolRS485.hpp"

class ProtocolModbus : public IProtocolRS485
{
public:
    ProtocolModbus();
    virtual ~ProtocolModbus() override;
    bool SetDeviceName(const std::string &device_name, unsigned char slaveid) override;
    bool SetDeviceConfiguration(int bd, char pr, int sb, int db, int xo) override;
    bool Initialize() override;
    bool Destroy() override;
    bool Read(unsigned int index, void** buffer) override;
    bool Write(unsigned int index, void* buffer) override;
    int GetQueryCount();
};

#endif
