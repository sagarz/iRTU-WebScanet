#include "ProtocolMODBUS.hpp"
#include <string.h>
#include <modbus/modbus.h>

#define MAX_MODBUS_QUERY (10)

#define GET_MODBUS_QUERY_RESP_SIZE(_arg_)   ((_arg_) == MODBUS_FC_READ_COILS) ? (sizeof(uint8_t)) :\
                                            ((_arg_) == MODBUS_FC_READ_DISCRETE_INPUTS) ? (sizeof(uint8_t)) : sizeof(uint16_t)

static char port_name[48] = {0};
static int baud_rate = 9600;
static char parity = 'N';
static char data_bits = 8;
static char stop_bits = 1;

#pragma pack(1)
typedef struct modbus_query
{
    unsigned char query_type;
    int start_address;
    int num_of_register;
}modbus_query;

static unsigned char slave_id = 0;
static unsigned int num_of_query = 0;
static modbus_query modbus_query_configuration[MAX_MODBUS_QUERY];
static unsigned int timeout_seconds;

static modbus_t *gsCtx = nullptr;

ProtocolModbus::ProtocolModbus()
{
}

ProtocolModbus::~ProtocolModbus()
{
}

bool ProtocolModbus::SetDeviceName(const std::string &device_name, unsigned char slaveid)
{
    memset(port_name, 0, 48);
    strcpy(port_name, device_name.c_str());
    slave_id = slaveid;
    return false;
}

bool ProtocolModbus::SetDeviceConfiguration(int bd, char pr, int sb, int db, int xo)
{
    baud_rate = bd;
    parity = pr;
    data_bits = db;
    stop_bits = sb;
    return true;
}

bool ProtocolModbus::Initialize()
{
    gsCtx = modbus_new_rtu(port_name, baud_rate, parity, data_bits, stop_bits);

    if (nullptr == gsCtx)
    {
        return false;
    }

    modbus_set_slave(gsCtx, slave_id);
    modbus_set_response_timeout(gsCtx, timeout_seconds, 0);

    if (-1 == modbus_connect(gsCtx))
    {
        modbus_free(gsCtx);
        return false;
    }

    num_of_query = 4;
    slave_id = 1;
    timeout_seconds = 7;

    modbus_query_configuration[0].start_address = 42;
    modbus_query_configuration[0].query_type = MODBUS_FC_READ_HOLDING_REGISTERS;
    modbus_query_configuration[0].num_of_register = 1;

    modbus_query_configuration[1].start_address = 48;
    modbus_query_configuration[1].query_type = MODBUS_FC_READ_HOLDING_REGISTERS;
    modbus_query_configuration[1].num_of_register = 1;

    modbus_query_configuration[2].start_address = 2400;
    modbus_query_configuration[2].query_type = MODBUS_FC_READ_HOLDING_REGISTERS;
    modbus_query_configuration[2].num_of_register = 10;

    modbus_query_configuration[3].start_address = 2450;
    modbus_query_configuration[3].query_type = MODBUS_FC_READ_HOLDING_REGISTERS;
    modbus_query_configuration[3].num_of_register = 10;

    return true;
}

bool ProtocolModbus::Destroy()
{
    modbus_close(gsCtx);
    modbus_free(gsCtx);
    return true;
}

bool ProtocolModbus::Read(unsigned int index, void** buffer)
{
    int retval = -1;
    modbus_query *query = nullptr;
    void *temp_buffer = nullptr;

    if(nullptr == buffer)
    {
        return false;
    }

    if(index > MAX_MODBUS_QUERY)
    {
        return false;
    }

    query = &(modbus_query_configuration[index]);

    (*buffer) = calloc(query->num_of_register, GET_MODBUS_QUERY_RESP_SIZE(query->query_type));

    if(nullptr == (*buffer))
    {
        return false;
    }

    temp_buffer = (*buffer);

    switch (query->query_type)
    {
        case MODBUS_FC_READ_COILS:
        {
            retval = modbus_read_bits(gsCtx, query->start_address, query->num_of_register, (uint8_t*)temp_buffer);

            if(-1 == retval)
            {
            }

            break;
        }
        case MODBUS_FC_READ_DISCRETE_INPUTS:
        {
            retval = modbus_read_input_bits(gsCtx, query->start_address, query->num_of_register, (uint8_t*)temp_buffer);

            if(-1 == retval)
            {
            }

            break;
        }
        case MODBUS_FC_READ_HOLDING_REGISTERS:
        {
            retval = modbus_read_registers(gsCtx, query->start_address, query->num_of_register, (uint16_t*)temp_buffer);

            if(-1 == retval)
            {
            }

            break;
        }
        case MODBUS_FC_READ_INPUT_REGISTERS:
        {
            retval = modbus_read_input_registers(gsCtx, query->start_address, query->num_of_register, (uint16_t*)temp_buffer);

            if(-1 == retval)
            {
            }

            break;
        }
#if 0
        case MODBUS_FC_WRITE_SINGLE_COIL:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_SINGLE_REGISTER:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_MULTIPLE_COILS:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
#endif
        default:
        {
        }
    }

    return true;
}

bool ProtocolModbus::Write(unsigned int index, void* buffer)
{
    return false;
}

int ProtocolModbus::GetQueryCount()
{
    return num_of_query;
}

