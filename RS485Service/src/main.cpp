#include "RS485Service.hpp"

int main(int argc, char* argv[])
{
    RS485Service serial_rs485;

    if(!serial_rs485.Initialize())
    {
        return -1;
    }

    serial_rs485.Start();

    return 0;
}
