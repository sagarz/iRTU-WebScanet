#include "AIOService.hpp"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <iRTUCore/MessageBus.h>
#include <iRTUCore/Logger.h>
#include <iRTUCore/SignalHandler.h>
#include <iRTUCore/File.h>
#include <iRTUCore/StringEx.h>

#include <string>
#include <thread>
#include <chrono>

static std::string payload_template = "{\"device_id\":\"000\", \"timestamp\":\"<timestamp>\", \"vibrationX\":\"<vibrationX>\", \"vibrationZ\":\"<vibrationZ>\", \"temperatureC\":\"<temperatureC>\"}";
static void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void OnSignalReceived(SignalType type);

static AIOService *appptr = nullptr;

AIOService::AIOService()
{
    message_bus = nullptr;
    logger_id = 1;
    appptr = this;
}

AIOService::~AIOService()
{

}

bool AIOService::Initialize()
{
    logger_id = logger_allocate(10,  nullptr);

    if(logger_id == SIZE_MAX)
    {
        return false;
    }

    if(!logger_start_logging(logger_id))
    {
        return false;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    char *config_file = (char*)calloc(2049, 1);
    config_file = getcwd(config_file, 2048);

    size_t pos = 0;
    pos = (size_t)strstr(config_file, "/bin");

    if(pos < 1)
    {
        for(int idx = strlen(config_file)-1; config_file[idx] != '/'; idx--)
        {
            config_file[idx] = 0;
        }
    }
    else
    {
        for(int idx = pos; idx <= 2049; idx++)
        {
            config_file[idx] = 0;
        }
    }

    strcat(config_file, "etc/AIOService.conf");

    if(!file_is_exists(config_file))
    {
        WriteLog(logger_id, "Configuration file not found", LOG_ERROR);
        return false;
    }

    bool res = cfg.LoadCustomConfiguration(config_file);
    free(config_file);

    if(!res)
    {
        WriteLog(logger_id, "Could not load configuration", LOG_ERROR);
        return false;
    }

    cfg.GetValueList("default", "destination", ',', destinationlist);

    return true;
}

bool AIOService::Destroy()
{
    return true;
}

bool AIOService::Start()
{
    if(!message_bus_initialize(&message_bus, OnNetworkEvent))
    {
        WriteLog(logger_id, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger_id, "Could not open IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_register(message_bus))
    {
        WriteLog(logger_id, "Could not register with the message bus", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    int ctr = 0;

    bool continue_loop = true;

    while(continue_loop)
    {
        std::this_thread::sleep_for (std::chrono::seconds(10));

        std::string payload = payload_template;
        std::string str;

        std::string timestamp = GetTimeStampString();
        timestamp.resize(timestamp.size() - 1);
        ReplaceString(payload, "<timestamp>", timestamp);

        double temperatureC = (double)(ctr%20)+50;
        str.clear();
        RealToString(str, temperatureC);
        ReplaceString(payload, "<temperatureC>", str);

        double vibrationX = (double)(ctr%5)+2;
        str.clear();
        RealToString(str, vibrationX);
        ReplaceString(payload, "<vibrationX>", str);

        double vibrationZ = (double)(ctr%25)+2;
        str.clear();
        RealToString(str, vibrationZ);
        ReplaceString(payload, "<vibrationZ>", str);

        for (auto destination : destinationlist)
        {
            long node_index = message_bus_has_node(message_bus, destination.c_str());

            if (node_index > -1)
            {
                long payload_id = 0;
                char* node_full_name = message_bus_node_fullname(message_bus, node_index);

                if(!message_bus_send(message_bus, node_full_name, Data, UserData, Text, payload.c_str(), payload.length(), &payload_id))
                {
                    continue_loop = false;
                    break;
                }

                if(!message_bus_send(message_bus, node_full_name, Event, UserData, Text, payload.c_str(), payload.length(), &payload_id))
                {
                    continue_loop = false;
                    break;
                }

                if(!message_bus_send(message_bus, node_full_name, Request, UserData, Text, payload.c_str(), payload.length(), &payload_id))
                {
                    continue_loop = false;
                    break;
                }

                if(!message_bus_send(message_bus, node_full_name, Response, UserData, Text, payload.c_str(), payload.length(), &payload_id))
                {
                    continue_loop = false;
                    break;
                }
            }
        }

        ctr++;

        if(ctr > 100)
        {
            ctr++;
        }
    }

    return true;
}

bool AIOService::Restart()
{
    return false;
}

bool AIOService::Stop()
{
    message_bus_deregister(message_bus);
    message_bus_close(message_bus);

    return false;
}

long AIOService::GetLogger()
{
    return logger_id;
}

void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    printf("%s %c %c %c %ld %s %ld\n", node_name, ptype, mtype, dtype, buffersize, messagebuffer, payload_id);
}

void OnSignalReceived(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(appptr->GetLogger(), "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(appptr->GetLogger(), "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(appptr->GetLogger(), "SHUTDOWN SIGNAL", LOG_CRITICAL);
            logger_stop_logging(appptr->GetLogger());
            exit(0);
        }
        case Alarm:
        {
            WriteLog(appptr->GetLogger(), "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(appptr->GetLogger(), "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(appptr->GetLogger(), "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(appptr->GetLogger(), "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
