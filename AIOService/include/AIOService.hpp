#ifndef ANALOG_IO_SERVICE
#define ANALOG_IO_SERVICE

#include <stdbool.h>
#include <string>
#include <vector>
#include "iRTUCommon.hpp"

class AIOService
{
public:
    AIOService();
    virtual ~AIOService();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    long GetLogger();
private:
    Configuration cfg;
    void* message_bus;
    long logger_id;
    std::vector<std::string> destinationlist;
};

#endif
