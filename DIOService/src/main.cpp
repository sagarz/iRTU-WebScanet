#include "DIOService.hpp"

int main(int argc, char* argv[])
{
    DIOService dio;

    if(!dio.Initialize())
    {
        return -1;
    }

    dio.Start();

    return 0;
}
