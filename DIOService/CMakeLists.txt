cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(Project DIOService)
project(${Project})

include_directories(./include/ ../Common/include /include/ /usr/include/ /usr/local/include/)
link_directories(/lib /usr/lib /usr/local/lib)
link_libraries(rt pthread dl irtucore)

set(SOURCE
${SOURCE}
./src/DIOService.cpp
../Common/src/iRTUCommon.cpp
./src/main.cpp
)

set(HEADERS
${HEADERS}
./include/DIOService.hpp
../Common/include/iRTUCommon.hpp
)

add_executable(${Project} ${SOURCE} ${HEADERS})
