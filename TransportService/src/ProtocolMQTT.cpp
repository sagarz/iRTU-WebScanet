#include "ProtocolMQTT.hpp"
#include <stdio.h>
#include <mosquitto.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static struct mosquitto *mosq = nullptr;
static int keepalive = 60*12*60;

ProtocolMQTT::ProtocolMQTT()
{
}

ProtocolMQTT::~ProtocolMQTT()
{
}

bool ProtocolMQTT::Initialize(const std::string &str_server, int num_port, IDownLink *dlink)
{
    port = num_port;
    server = str_server;
    downlink = dlink;

    bool clean_session = true;

    mosquitto_lib_init();

    mosq = mosquitto_new(nullptr, clean_session, nullptr);

    if(!mosq)
    {
        return false;
    }

    return true;
}

bool ProtocolMQTT::Connect()
{
    if(mosquitto_connect(mosq, server.c_str(), port, keepalive) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    int loop = mosquitto_loop_start(mosq);

    if(loop != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}

bool ProtocolMQTT::SendData(const std::string &str, const std::string &uri)
{
    std::string topic = uri + "/data/+";

    if(mosquitto_publish(mosq, nullptr, topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}

bool ProtocolMQTT::SendResponse(const std::string &str, const std::string &uri)
{
    std::string topic = uri + "/response/+";

    if(mosquitto_publish(mosq, nullptr, topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}

bool ProtocolMQTT::SendRequest(const std::string &str, const std::string &uri)
{
    std::string topic = uri + "/request/+";

    if(mosquitto_publish(mosq, nullptr, topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}

bool ProtocolMQTT::SendEvent(const std::string &str, const std::string &uri)
{
    std::string topic = uri + "/event/+";

    if(mosquitto_publish(mosq, nullptr, topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}



