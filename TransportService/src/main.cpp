#include "TransportService.hpp"

int main(int argc, char* argv[])
{
    TransportService service;

    if(!service.Initialize())
    {
        return -1;
    }

    service.Start();

    return 0;
}
