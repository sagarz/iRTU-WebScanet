#include "TransportService.hpp"
#include "ProtocolHTTP.hpp"
#include "ProtocolMQTT.hpp"

#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <algorithm>
#include <thread>
#include <chrono>
#include <iostream>
#include <string>

#include <iRTUCore/MessageBus.h>
#include <iRTUCore/Logger.h>
#include <iRTUCore/SignalHandler.h>
#include <iRTUCore/StringEx.h>
#include <iRTUCore/StringList.h>
#include <iRTUCore/File.h>

static void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void OnSignalReceived(SignalType type);

static TransportService *appptr = nullptr;
static IServiceCallback *callback = nullptr;

TransportService::TransportService()
{
    inet_protocol = nullptr;
    message_bus = nullptr;
    logger_id = -1;
    appptr = this;
    callback = this;
}

TransportService::~TransportService()
{

}

bool TransportService::Initialize()
{
    logger_id = logger_allocate(10,  nullptr);

    if(logger_id == SIZE_MAX)
    {
        return false;
    }

    if(!logger_start_logging(logger_id))
    {
        return false;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    char *config_file = (char*)calloc(2049, 1);
    config_file = getcwd(config_file, 2048);

    size_t pos = 0;
    pos = (size_t)strstr(config_file, "/bin");

    if(pos < 1)
    {
        for(int idx = strlen(config_file)-1; config_file[idx] != '/'; idx--)
        {
            config_file[idx] = 0;
        }
    }
    else
    {
        for(int idx = pos; idx <= 2049; idx++)
        {
            config_file[idx] = 0;
        }
    }

    strcat(config_file, "etc/TransportService.conf");

    if(!file_is_exists(config_file))
    {
        WriteLog(logger_id, "Configuration file not found", LOG_ERROR);
        return false;
    }

    bool res = cfg.LoadCustomConfiguration(config_file);
    free(config_file);

    if(!res)
    {
        WriteLog(logger_id, "Could load configuration", LOG_ERROR);
        return false;
    }

    cfg.GetValue("General", "server",server_name);
    cfg.GetValue("General", "protocol", protocol_name);
    cfg.GetValue("General", "uri", server_uri);
    cfg.GetValue("General", "device_id", device_id);
    cfg.GetValue("General", "header", header_template);

    std::string temp;
    cfg.GetValue("General", "port", temp);
    server_port = atoi(temp.c_str());

    cfg.GetValue("General", "enabled", temp);
    enabled = (bool)atoi(temp.c_str());

    cfg.GetValue("General", "latitude", temp);
    latitude = atof(temp.c_str());

    cfg.GetValue("General", "longitude", temp);
    longitude = atof(temp.c_str());

    return true;
}

bool TransportService::Start()
{
    if(enabled)
    {
        if(protocol_name == "http")
        {
            inet_protocol = new ProtocolHTTP();
        }
        else
        {
            if(protocol_name == "mqtt")
            {
                inet_protocol = new ProtocolMQTT();
            }
            else
            {
                return false;
            }
        }

        if(!inet_protocol->Initialize(server_name, server_port, this))
        {
            return false;
        }

        if(!inet_protocol->Connect())
        {
            return false;
        }
    }

    if(!message_bus_initialize(&message_bus, OnNetworkEvent))
    {
        WriteLog(logger_id, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger_id, "Could not open IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_register(message_bus))
    {
        WriteLog(logger_id, "Could not register with the message bus", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    while(true)
    {
        std::this_thread::sleep_for (std::chrono::seconds(10));
    }

    return true;
}

bool TransportService::ReStart()
{
    return false;
}

bool TransportService::Stop()
{
    message_bus_deregister(message_bus);
    message_bus_close(message_bus);

    return false;
}

long TransportService::GetLogger()
{
    return logger_id;
}

bool TransportService::OnDownLink(const std::string &str)
{

}

void TransportService::OnNodeOnline(const std::string &nodename)
{
    std::string str = nodename + " online";
    WriteInformation(logger_id, str.c_str());
}

void TransportService::OnNodeOffline(const std::string &nodename)
{
    std::string str = nodename + " offline";
    WriteInformation(logger_id, str.c_str());
}

void TransportService::OnData(const std::string &nodename, const std::string &messagebuffer)
{
    WriteInformation(logger_id, "Received data");

    if(inet_protocol)
    {
        std::string payload = header_template;
        ReplaceString(payload, "<message_type>", "data");
        Upload(payload, nodename, messagebuffer);
        inet_protocol->SendData(payload, server_uri);
    }
}

void TransportService::OnEvent(const std::string &nodename, const std::string &messagebuffer)
{
    WriteInformation(logger_id, "Received event");

    if(inet_protocol)
    {
        std::string payload = header_template;
        ReplaceString(payload, "<message_type>", "event");
        Upload(payload, nodename, messagebuffer);
        inet_protocol->SendEvent(payload, server_uri);
    }
}

void TransportService::OnRequest(const std::string &nodename, const std::string &messagebuffer)
{
    WriteInformation(logger_id, "Received request");

    if(inet_protocol)
    {
        std::string payload = header_template;
        ReplaceString(payload, "<message_type>", "request");
        Upload(payload, nodename, messagebuffer);
        inet_protocol->SendRequest(payload, server_uri);
    }
}

void TransportService::OnResponse(const std::string &nodename, const std::string &messagebuffer)
{
    WriteInformation(logger_id, "Received response");

    if(inet_protocol)
    {
        std::string payload = header_template;
        ReplaceString(payload, "<message_type>", "response");
        Upload(payload, nodename, messagebuffer);
        inet_protocol->SendResponse(payload, server_uri);
    }
}

void TransportService::Upload(std::string &str, const std::string &nodename, const std::string &messagebuffer)
{
    std::string ts = GetTimeStampString();

    std::string temp;

    RealToString(temp, longitude);
    ReplaceString(str, "<longitude>", temp);

    RealToString(temp, latitude);
    ReplaceString(str, "<latitude>", temp);

    ReplaceString(str, "<transport_type>", protocol_name);
    ReplaceString(str, "<device_id>", device_id);
    ReplaceString(str, "<application_id>", nodename);
    ReplaceString(str, "<timestamp>", ts);
    ReplaceString(str, "<payload>", messagebuffer);
}

void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    if(ptype == Data)
    {
        if(mtype != LoopBack)
        {
            if(callback)
            {
                callback->OnData(node_name, messagebuffer);
            }
        }
    }

    if(ptype == Response)
    {
        if(callback)
        {
            callback->OnResponse(node_name, messagebuffer);
        }
    }

    if(ptype == Request)
    {
        if(callback)
        {
            callback->OnRequest(node_name, messagebuffer);
        }
    }

    if(ptype == Event)
    {
        if(mtype == NodeOnline)
        {
            if(callback)
            {
                callback->OnNodeOnline(messagebuffer);
            }
        }
        else
        {
            if(mtype == NodeOffline)
            {
                if(callback)
                {
                    callback->OnNodeOffline(messagebuffer);
                }
            }
            else
            {
                if(callback)
                {
                    callback->OnEvent(node_name, messagebuffer);
                }
            }
        }
    }
}

void OnSignalReceived(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(appptr->GetLogger(), "SUSPEND SIGNAL", LOG_WARNING);
            break;
        }
        case Resume:
        {
            WriteLog(appptr->GetLogger(), "RESUME SIGNAL", LOG_WARNING);
            break;
        }
        case Shutdown:
        {
            WriteLog(appptr->GetLogger(), "SHUTDOWN SIGNAL", LOG_CRITICAL);
            logger_stop_logging(appptr->GetLogger());
            exit(0);
        }
        case Alarm:
        {
            WriteLog(appptr->GetLogger(), "ALARM SIGNAL", LOG_WARNING);
            break;
        }
        case Reset:
        {
            WriteLog(appptr->GetLogger(), "RESET SIGNAL", LOG_WARNING);
            break;
        }
        case ChildExit:
        {
            WriteLog(appptr->GetLogger(), "CHILD PROCESS EXIT SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined1:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 1 SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined2:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 2 SIGNAL", LOG_WARNING);
            break;
        }
        default:
        {
            WriteLog(appptr->GetLogger(), "UNKNOWN SIGNAL", LOG_WARNING);
            break;
        }
    }
}


