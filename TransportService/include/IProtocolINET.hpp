#ifndef I_PROTOCOL_INET
#define I_PROTOCOL_INET

#include <string>

class IDownLink
{
public:
    IDownLink() {}
    virtual ~IDownLink() {}
    virtual bool OnDownLink(const std::string &str) = 0;
};

class IProtocolINET
{
public:
    IProtocolINET() {}
    virtual ~IProtocolINET() {}
    virtual bool Initialize(const std::string &str_server, int num_port, IDownLink* dlink = nullptr) = 0;
    virtual bool Connect() = 0;
    virtual bool SendData(const std::string &str, const std::string &uri) = 0;
    virtual bool SendResponse(const std::string &str, const std::string &uri) = 0;
    virtual bool SendRequest(const std::string &str, const std::string &uri) = 0;
    virtual bool SendEvent(const std::string &str, const std::string &uri) = 0;
protected:
   std::string server;
   int port;
   IDownLink* downlink;
};

#endif
