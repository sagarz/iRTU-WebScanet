#ifndef TRANSPORT_SERVICE
#define TRANSPORT_SERVICE

#include "IProtocolINET.hpp"
#include "iRTUCommon.hpp"

class IServiceCallback
{
public:
    IServiceCallback() {}
    virtual ~IServiceCallback() {}
    virtual void OnNodeOnline(const std::string &nodename) =0;
    virtual void OnNodeOffline(const std::string &nodename) =0;
    virtual void OnData(const std::string &nodename, const std::string &messagebuffer) =0;
    virtual void OnEvent(const std::string &nodename, const std::string &messagebuffer) =0;
    virtual void OnRequest(const std::string &nodename, const std::string &messagebuffer) =0;
    virtual void OnResponse(const std::string &nodename, const std::string &messagebuffer) =0;
};

class TransportService : public IServiceCallback, IDownLink
{
public:
    TransportService();
    virtual ~TransportService() override;
    bool Initialize();
    bool Start();
    bool ReStart();
    bool Stop();
    long GetLogger();
protected:
    void OnNodeOnline(const std::string &nodename) override;
    void OnNodeOffline(const std::string &nodename) override;
    void OnData(const std::string &nodename, const std::string &messagebuffer) override;
    void OnEvent(const std::string &nodename, const std::string &messagebuffer) override;
    void OnRequest(const std::string &nodename, const std::string &messagebuffer) override;
    void OnResponse(const std::string &nodename, const std::string &messagebuffer) override;
    bool OnDownLink(const std::string &str) override;
private:
    void Upload(std::string &str, const std::string &nodename, const std::string &messagebuffer);
    Configuration cfg;
    void* message_bus;
    std::string protocol_name;
    std::string server_name;
    int server_port;
    std::string server_uri;
    std::string device_id;
    bool enabled;
    IProtocolINET *inet_protocol;
    long logger_id;
    std::string header_template;
    double latitude;
    double longitude;
};

#endif
