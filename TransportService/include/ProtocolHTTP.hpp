#ifndef PROTOCOL_HTTP
#define PROTOCOL_HTTP

#include "IProtocolINET.hpp"

class ProtocolHTTP : public IProtocolINET
{
public:
    ProtocolHTTP();
    virtual ~ProtocolHTTP() override;
    bool Initialize(const std::string &str_server, int num_port, IDownLink* dlink = nullptr) override;
    bool Connect() override;
    bool SendData(const std::string &str, const std::string &uri) override;
    bool SendResponse(const std::string &str, const std::string &uri) override;
    bool SendRequest(const std::string &str, const std::string &uri) override;
    bool SendEvent(const std::string &str, const std::string &uri) override;
};


#endif
