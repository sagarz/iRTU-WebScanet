#ifndef WIFI_SERVICE
#define WIFI_SERVICE

#include <stdbool.h>
#include <string>
#include <vector>
#include "iRTUCommon.hpp"

class WiFiService
{
public:
    WiFiService();
    virtual ~WiFiService();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    long GetLogger();
private:
    Configuration cfg;
    void* message_bus;
    long logger_id;
    std::vector<std::string> destinationlist;
};

#endif
